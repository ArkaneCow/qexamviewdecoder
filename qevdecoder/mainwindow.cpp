#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <string>
#include <QDebug>
#include <sstream>
#include <QtNetwork/QNetworkAccessManager>
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>
#include <QIODevice>
#include <QUrl>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->setCentralWidget(ui->verticalLayoutWidget);
    ui->textEdit->setText("None");
    ui->textEdit->setReadOnly(true);
    this->showFullScreen();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::process_page(QNetworkReply* reply)
{
    QString quiz_html = reply->readAll();
    QStringList answer_list;
    QStringList source_split = quiz_html.split("ansMap[");
    for (int i = 0; i < source_split.count(); i++)
    {
        if (i != 0 && i != source_split.count() && i != source_split.count() -1 && i != source_split.count() -2)
        {
            QString raw_split = source_split[i];
            QStringList first_split = raw_split.split("';");
            QString first = first_split[0];
            QStringList second_split = first.split("] = '");
            QString ans_index = second_split[0];
            QString s = second_split[1];
            int ans_index_int = ans_index.toInt();
            int value = (ans_index_int % 31) + 1;
            for (int j = 0; j < s.count(); j = j + 2)
            {
                const char* code_char = (s.mid(j, j+2)).toStdString().c_str();
                char* p_end;
                int code = strtol(code_char, &p_end, 16);
                QString ans_val = QString((char)(code^value));
                qDebug() << ans_val << endl;
                answer_list.push_back(ans_val);
            }
        }
    }
    QString text_ans = "";
    for (int k = 0; k < answer_list.count(); k++)
    {
        stringstream ss;
        ss << (k+1);
        string s;
        ss >> s;
        text_ans += QString::fromStdString(s) + QString(") ") + answer_list[k] + QString("\n");
    }
    ui->textEdit->setText(text_ans);
}

void MainWindow::decode_quiz()
{
    ui->textEdit->setText("Loading...");
    QNetworkAccessManager *nam = new QNetworkAccessManager(this);
    connect(nam, SIGNAL(finished(QNetworkReply*)), this, SLOT(process_page(QNetworkReply*)));
    QNetworkReply* reply = nam->get(QNetworkRequest(QUrl(ui->lineEdit->text())));
}
